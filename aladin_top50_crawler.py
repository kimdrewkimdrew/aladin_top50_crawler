# -*- coding: UTF-8 -*-
# Python, BeatifulSoup4, selenium, codecs, chrome_driver 가 필요합니다.

"""

설치법  
BeatifulSoup : pip install bs4
selenium     : pip install selenium
codecs       : pip install codecs
chromedriver : https://sites.google.com/a/chromium.org/chromedriver/downloads 
이 네가지를 설치해주세요.

"""

from selenium import webdriver
import codecs
from bs4 import BeautifulSoup
import time

driver = webdriver.Chrome("E:/driver/chromedriver.exe")     # chromedriver.exe를 다운받고 그 경로를 적어주세요.
driver.get("http://www.aladin.co.kr/")      

fw=codecs.open("E:/driver/result.txt","w","utf-8")     # 첫번째칸에 결과를 어디에 저장할건지 경로를 지정해주세요.

time.sleep(2)

driver.find_element_by_xpath("//*[@id=\"re_mallmenu\"]/ul/li[3]/div/a/img").click()

time.sleep(2)

p_source=driver.page_source

bs = BeautifulSoup(p_source, "html.parser")
bs2 = bs.find_all("form", id="Myform")
bs3 = BeautifulSoup(str(bs2), "html.parser")    
bs4 = bs3.find_all("div", class_="ss_book_box")

idx = 1

for i in bs4:
    i2=i.find_all("li")
    print(len(i2))
    
    print("=========================================================")
    idx2 = 1
    t50list = []
    for j in i2:
        if idx2 == 1 or idx2 == 2 or idx2 == 3 or idx2 == 4:
            t50list.append(j.text)    
        idx2 = idx2+1
    
    if t50list[0].count("[")>0:           
        t50list.pop(0)
    else:
        t50list.pop(3)
    for t50 in t50list:
        print(t50)
        fw.write(str(t50)+"\r\n")
    fw.write("=====================================================\r\n\n")
    idx = idx+1
    
    print("=========================================================")
driver.close()
